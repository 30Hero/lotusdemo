package com.lotus.demo.ui.activities;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lotus.demo.R;
import com.lotus.demo.ui.fragments.BaseFragment;
import com.lotus.demo.ui.fragments.ConnectFragment;
import com.lotus.demo.ui.fragments.ProfileFragment;
import com.lotus.demo.ui.fragments.SettingFragment;

public class MainActivity extends BaseActivity {

    private BottomNavigationView bottomNavigation;

    private ActionBar toolbar;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        bottomNavigation = findViewById(R.id.bottom_navigation);
        toolbar = getSupportActionBar();
    }

    @Override
    protected void main() {
        bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        openFragment(new ConnectFragment());
        toolbar.setTitle(getResources().getString(R.string.profile_fragment_title));
    }

    public void openFragment(BaseFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.navigation_profile:
                            toolbar.setTitle(getResources().getString(R.string.profile_fragment_title));
                            openFragment(new ProfileFragment());
                            return true;
                        case R.id.navigation_connect:
                            toolbar.setTitle(getResources().getString(R.string.connect_fragment_title));
                            openFragment(new ConnectFragment());
                            return true;
                        case R.id.navigation_setting:
                            toolbar.setTitle(getResources().getString(R.string.setting_fragment_title));
                            openFragment(new SettingFragment());
                            return true;
                    }
                    return false;
                }
            };
}