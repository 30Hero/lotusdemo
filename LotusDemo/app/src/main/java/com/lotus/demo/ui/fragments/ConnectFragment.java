package com.lotus.demo.ui.fragments;

import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.lotus.demo.R;
import com.lotus.demo.ui.activities.CallActivity;


public class ConnectFragment  extends BaseFragment {

    @Override
    protected int getLayout() {
        return R.layout.fragment_connect;
    }

    private Button btnConnect;

    @Override
    protected void initViews() {
        btnConnect = (Button) view.findViewById(R.id.btnConnect);
    }

    @Override
    protected void main() {
        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CallActivity.class));
            }
        });
    }
}